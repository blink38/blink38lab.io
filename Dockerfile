FROM registry.gitlab.com/pages/hugo/hugo_extended:latest

LABEL maintainer="Matthieu MARC"

RUN apk add vim patch git

EXPOSE 1313

WORKDIR /src/blog
VOLUME /src/blog

ENTRYPOINT ["hugo","serve","--bind=0.0.0.0"]