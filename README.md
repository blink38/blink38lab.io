Docker
======

build docker image :

```
docker build . -t blog
```


run docker image :

```
docker run --rm -it -p 1313:1313 -v ${PWD}:/src/blog blog
```