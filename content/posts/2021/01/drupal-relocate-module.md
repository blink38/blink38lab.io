---
title: "Mettre à jour le chemin d'un module drupal"
date: 2021-01-14T09:30:41Z
categories: [dev]
tags: [drupal, dev]
draft: false
---

Suite à la migration de mon projet Drupal et la modification **drupal-scaffold**, les modules installés dans le répertoire ```web/sites/monsite/modules/crontrib``` se sont retrouvés dupliqués dans ```web/modules/contrib```. Ceci s'explique par le fait que la configuration *drupal_scaffold* que j'avais mis en place dans *composer.json* indiquait que le chemin des modules drupal (```type:drupal-module```) était ```web/modules/contrib```.

Lorsque j'exécutais ```drush cr```, j'avais une erreur de re-déclaration de mes modules (puisqu'ils étaient à deux endroits).

J'ai mis à jour mon *composer.json* pour lui indiquer le chemin des modules drupal, mais ça ne suffit pas, il faut, après avoir supprimé les modules en double, exécutez la commande suivante pour mettre à jour l'autoload composer :

```
composer dump-autoload
```

Ainsi, le chargement des modules est mis à jour. Cette command est à exécuter à la racine de votre projet drupal. Vider le cache de Drupal n'est pas suffisant.
